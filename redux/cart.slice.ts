import { createSlice } from '@reduxjs/toolkit';
import Product from "@models/product.model";

/** Possible actions in the cart store */
type CartActions = '' | 'addToCart' | 'clearCart';

/** Interface to use product but with quantity for store */
export interface CartEntry {
    product: Product
    quantity: number
}
export interface CartStore {
    entries: CartEntry[]
    lastAction: CartActions
}

/** Initial State of the Cart Store */
const initialState: CartStore = {
    entries: [],
    lastAction: ''
}

const cartSlice = createSlice({
    name: 'cart',
    initialState: initialState,
    reducers: {
        addToCart: (state, action) => {
            // Finds the item in the state
            const item = state.entries.find((item) => item.product.id === action.payload.id);
            // Updates the quantity to increment it if it exists
            // else add it to the state
            if (item) {
                item.quantity++;
            } else {
                const entry: CartEntry = {product: action.payload, quantity: 1}
                state.entries.push(entry);
            }
            state.lastAction = 'addToCart'
        },
        clearCart: (state) => {
            // Clears all entries from the cart
            state.entries.splice(0, state.entries.length)
            state.lastAction = 'clearCart'
        },
        resetLastAction: (state) => {
            state.lastAction = ''
        }
    },
});

export const cartReducer = cartSlice.reducer;

export const {
    addToCart,
    clearCart,
    resetLastAction
} = cartSlice.actions;
