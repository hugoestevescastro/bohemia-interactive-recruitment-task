import { configureStore } from '@reduxjs/toolkit';
import {cartReducer, CartStore} from './cart.slice';

const reducer = {
    cart: cartReducer,
};

const store = configureStore({
    reducer,
});

export interface State {
    cart: CartStore
}

export default store;
