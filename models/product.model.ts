export default interface Product {
    id: number,
    name: string,
    category: string,
    price: number,
    currency: string,
    image: ProductImage,
    bestseller: boolean,
    featured: boolean,
    details: ProductDetails
}

interface ProductImage {
    src: string,
    alt: string
}
interface ProductDimensions {
    width: number,
    height: number
}
interface ProductDetails {
    dimensions: ProductDimensions,
    size: number,
    description: string,
    recommendations: Array<number>
}
