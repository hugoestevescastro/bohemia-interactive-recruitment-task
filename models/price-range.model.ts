/** Model for the options of PriceRange */
export default interface PriceRange {
    currency: string,
    start?: number,
    end?: number
}
