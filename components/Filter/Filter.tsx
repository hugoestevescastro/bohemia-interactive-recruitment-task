import {FC, useEffect, useState} from 'react'
import PriceRange from "@models/price-range.model";
import {v4 as uuid} from "uuid";
import styles from "./Filter.module.sass"
import GetCurrencySymbol from "@hooks/GetCurrencySymbol";

interface Props {
    priceRanges: PriceRange[]
    categories: string[],
    onFilter: Function,
    clear?: boolean
}

export interface OnFilterModel {
    priceRange: PriceRange | undefined,
    categories: string[]
}

const Filter: FC<Props> = ({priceRanges, categories, onFilter, clear}) => {
    const [selectedPriceRange, setSelectedPriceRange] = useState<PriceRange>();
    const [selectedCategories, setSelectedCategories] = useState<string[]>([])
    useEffect(() => {
        const filter: OnFilterModel = {
            priceRange: selectedPriceRange,
            categories: selectedCategories
        }
        onFilter(filter);
    }, [selectedCategories, selectedPriceRange])

    /** Reset filters if asked to */
    useEffect(() => {
        if (clear) {
            setSelectedPriceRange(undefined);
            setSelectedCategories([])
        }
    }, [clear])
    return (
        <div>
            <h1 className={'font-bold'}>Category</h1>
            <ul className={'py-8 border-b-2 border-gray-300'}>
                {categories && categories.length > 0 && categories.map(category => {
                    return <li className={'capitalize mb-4 last:mb-0'} key={uuid()}>
                        <label className={styles.checkbox}>
                            <input
                                type='checkbox'
                                checked={selectedCategories.includes(category)}
                                onChange={() => {
                                    let state;
                                    // if exists, must be removed, else add
                                    if (selectedCategories.includes(category)) {
                                        // get index of existing
                                        const i = selectedCategories.indexOf(category);
                                        // clone the array to trigger a state change, react rule
                                        state = [...selectedCategories]
                                        // remove the element from it, only if it exists
                                        i > -1 && state.splice(i, 1);
                                    } else {
                                        state = [...selectedCategories, category]
                                    }
                                    // set new state
                                    setSelectedCategories(state);
                                }} />
                            <span></span>
                            {category}
                        </label>
                    </li>
                })}
            </ul>
            <h1 className={'font-bold pt-8'}>Price range</h1>
            <ul className={'pt-8'}>
                {priceRanges && priceRanges.length > 0 && priceRanges.map(priceRange => {
                    return <li className={'capitalize mb-4'} key={uuid()}>
                        <label className={styles.checkbox}>
                            <input
                                type='checkbox'
                                checked={selectedPriceRange == priceRange}
                                onChange={() => {
                                        if (selectedPriceRange == priceRange) {
                                            setSelectedPriceRange(undefined);
                                        } else {
                                            setSelectedPriceRange(priceRange)
                                        }
                                    }
                                } />
                            {/* This span is used as indicator placeholder */}
                            <span></span>
                            {/* First price range, where goes from 0 to X */}
                            {!priceRange.start && priceRange.end &&
                                <>
                                    Lower than {GetCurrencySymbol(priceRange.currency)}{priceRange.end}
                                </>
                            }
                            {/* Regular price range with start and end */}
                            {priceRange.start && priceRange.end &&
                                <>
                                    {GetCurrencySymbol(priceRange.currency)}{priceRange.start} - {GetCurrencySymbol(priceRange.currency)}{priceRange.end}
                                </>
                            }
                            {/* Last price range, where goes from X to infinite */}
                            {priceRange.start && !priceRange.end &&
                                <>
                                    More than {GetCurrencySymbol(priceRange.currency)}{priceRange.start}
                                </>
                            }
                        </label>
                    </li>
                })}
            </ul>
        </div>
    )

}

export default Filter
