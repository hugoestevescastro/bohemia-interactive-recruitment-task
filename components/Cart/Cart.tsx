import {FC, useEffect, useState} from 'react'
import { Cart as CartIcon, Cross } from "@components/icons";
import { useSelector, useDispatch } from "react-redux";
import { State } from "@redux/store";
import styles from './Cart.module.sass'
import {v4 as uuid} from "uuid";
import {CartItem} from "@components/Cart/CartItem";
import Button from "@components/Button";
import {clearCart, resetLastAction} from "@redux/cart.slice";

interface Props {
}

const Cart: FC<Props> = ({}) => {
    const [isDropdownOpen, setIsDropdownOpen] = useState(false);

    // Extracting last action from the store
    const lastAction = useSelector((state: State) => {
        return state.cart.lastAction
    })

    /** If the last action was add to cart, should open the cart,
     *  Else if it was clear cart, should close it
     **/
    useEffect(() => {
        if (lastAction == 'addToCart') {
            setIsDropdownOpen(true)
        } else if (lastAction == 'clearCart') {
            setIsDropdownOpen(false)
        }
        dispatch(resetLastAction())
    },[lastAction])

    // Extracting cart from store
    const cart = useSelector((state: State) => {
        return state.cart.entries
    });

    // Reference to redux's dispatch
    const dispatch = useDispatch();

    /**
     * Used to count all the items' quantity in the cart
     * @returns a number containing all items in the cart
     */
    const countCartItems = () => {
        return cart.reduce((accumulator, item) => accumulator + item.quantity, 0)
    }
    return (
        <div className={"relative"}>
            <div className={`cursor-pointer`} onClick={() => setIsDropdownOpen(!isDropdownOpen)}>
                <CartIcon className={"w-full"} />
                {/* Item counter below the icon */}
                <div
                    className={"absolute bottom-1 -right-2 text-white bg-black text-xs px-1"}>
                    {countCartItems()}
                </div>
            </div>
            {/* Dropdown menu */}
            <div className={`
                ${isDropdownOpen ? `h-60 opacity-100 pointer-events-auto` : `h-0 opacity-0 pointer-events-none ${styles.closed}`} ${styles.cart}
                z-50 absolute top-full mt-2 right-0 w-[calc(100vw-4rem)] lg:w-[30vw] border-gray-300 border-2 bg-white
                grid grid-cols-5
                `}>
                {/* Close button */}
                <div className={`col-span-5 justify-self-end p-2`} onClick={() => setIsDropdownOpen(false)}>
                    <Cross className={`w-4 cursor-pointer`} />
                </div>
                {/* Cart items listing */}
                <div className={'col-span-5 overflow-auto border-b-2 border-gray-300'}>
                    {cart && cart.length > 0 && cart.map(item => {
                        return <CartItem cartEntry={item} key={uuid()} />
                    })}
                    {!cart || cart.length == 0 && <>
                        <p className={'text-center text-xl'}>Your cart is empty!</p>
                    </>}
                </div>
                {/* Clear cart items btn */}
                { cart && cart.length > 0 &&
                    <div className={'col-span-5 px-4 py-2'}>
                        <Button type={"submit"} text={"clear"} onClick={() => {dispatch(clearCart())}} inverted={true}></Button>
                    </div>
                }
            </div>
        </div>
    )

}

export default Cart
