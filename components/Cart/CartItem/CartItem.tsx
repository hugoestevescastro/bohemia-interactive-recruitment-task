import {FC} from 'react'
import {v4 as uuid} from "uuid";
import Image from "next/image";
import Product from "@models/product.model";
import {CartEntry} from "@redux/cart.slice";
import GetCurrencySymbol from "@hooks/GetCurrencySymbol";

interface Props {
    cartEntry: CartEntry
}

const CartItem: FC<Props> = ({cartEntry}) => {
    const product: Product = cartEntry.product;

    return (
        <div className={'flex px-4 h-16 mb-4'} key={uuid()}>
            <div className={'w-3/5'}>
                <p className={'text-black font-bold text-base'}>({cartEntry.quantity}) {product.name}</p>
                <p className={'text-gray-400 font-normal text-lg'}>{GetCurrencySymbol(product.currency)}{(product.price * cartEntry.quantity).toFixed(2)}</p>
            </div>
            <div className={"w-2/5 relative"}>
                <Image
                    src={product.image.src}
                    alt={product.image.alt}
                    fill={true}
                    style={{objectFit: 'cover'}} />
            </div>
        </div>
    )

}

export default CartItem
