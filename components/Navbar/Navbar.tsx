import { FC } from 'react'
import { Logo } from "@components/icons";
import Link from "next/link";
import Cart from "@components/Cart";

const Navbar: FC = () => {
    return (
        <nav className={"w-full flex flex-row justify-between border-b-2 border-gray-300 items-center pb-2 sticky top-0 left-0 z-[100] bg-white"}>
            <Link href={'/'}>
                <Logo className={"w-28"} />
            </Link>
            <div className={"w-7"}>
                <Cart />
            </div>
        </nav>
    )

}

export default Navbar
