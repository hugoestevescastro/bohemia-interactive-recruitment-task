import {FC} from 'react'
import Product from "@models/product.model";
import Image from "next/image";
import GetCurrencySymbol from "@hooks/GetCurrencySymbol";
import UseHover from "@hooks/UseHover";
import { useDispatch } from "react-redux";
import { addToCart } from "@redux/cart.slice";

interface Props {
    product: Product
}
const ProductCard: FC<Props> = ({product}) => {
    const [hoverRef, isHovered] = UseHover();
    const dispatch = useDispatch();

    return (
        <div ref={hoverRef} className={'relative'}>
            {product.bestseller && <div className={'text-black bg-white absolute top-0 left-0 text-base px-4 z-50'}>
                Best Seller
            </div>}
            <div className={'w-full h-[450px] relative'}>
                <Image
                    src={product.image.src}
                    alt={product.image.alt}
                    fill={true}
                    style={{objectFit: 'cover'}}
                />
                {/* Hover add to cart div*/}
                <div
                    className={`uppercase text-center text-lg px-4 py-2 cursor-pointer absolute bottom-0 left-0 w-full bg-black text-white transition-all duration-300 origin-bottom
                    ${isHovered ? 'scale-y-100 opacity-100' : 'scale-y-0 opacity-0'}`}
                    onClick={() => dispatch(addToCart(product))}
                >
                    add to cart
                </div>
            </div>
            <h2 className={'capitalize text-base text-gray-400 font-bold'}>{product.category}</h2>
            <h1 className={'capitalize text-xl text-black font-bold'}>{product.name}</h1>
            <p className={'font-normal text-lg text-gray-400'}>{GetCurrencySymbol(product.currency)}{product.price.toFixed(2)}</p>
        </div>
    )

}

export default ProductCard
