import {FC, useEffect, useState} from 'react'
import Product from "@models/product.model";
import {ProductCard} from "@components/ProductList/ProductCard";
import {v4 as uuid} from "uuid";
import {Arrow} from "@components/icons";

interface Props {
    products: Product[]
}
const ProductList: FC<Props> = ({products}) => {
    const pageSize = 6; // Default page size, could add select option to change?
    const [currentPage, setCurrentPage] = useState(0);
    const [pagedProducts, setPagedProducts] = useState<Product[][]>();

    // Everytime products prop change, the pages products need to be "re-paged"
    // Including at the beginning
    useEffect(()=> {
        const pages = [];
        for (let i = 0; i < products.length; i += pageSize) {
            pages.push(products.slice(i, i + pageSize))
        }
        setPagedProducts(pages)

    },[products])

    return (
        <div className={'grid grid-cols-1 lg:grid-cols-3 gap-6'}>
            {pagedProducts && pagedProducts.length > 0 && pagedProducts[currentPage].map(product => {
                return <ProductCard key={uuid()} product={product} />
            })}
            {/* This is the paginator */}
            {pagedProducts && pagedProducts.length > 0 && <div className={'lg:col-span-3 flex justify-center gap-6 items-center'}>
                {currentPage > 0 && <span
                    className={'cursor-pointer'}
                    onClick={() => {
                        currentPage > 0 && setCurrentPage(currentPage - 1)
                    }}
                >
                    <Arrow />
                </span>}
                {Array.from({length: pagedProducts.length}, (v, k) => k).map(page => {
                    return (
                        <span
                            className={`text-lg ${page == currentPage ? 'text-black' : 'text-gray-400 cursor-pointer'}`}
                            onClick={() => setCurrentPage(page)}
                            key={uuid()}
                        >
                            {page + 1}
                        </span>
                    )
                })
                }
                {currentPage < pagedProducts.length - 1 && <span
                    className={'cursor-pointer'}
                    onClick={() => {
                        pagedProducts && currentPage < pagedProducts.length - 1 && setCurrentPage(currentPage + 1)
                    }}
                >
                    <Arrow className={'rotate-180'} />
                </span>}
            </div>}
        </div>
    )

}

export default ProductList
