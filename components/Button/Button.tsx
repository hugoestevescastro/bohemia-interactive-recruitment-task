import { FC } from 'react'
import styles from './Button.module.sass'

type ButtonType = "button" | "submit" | "reset" | undefined;
interface Props {
    text: string,
    type: ButtonType,
    onClick: Function,
    inverted?: boolean
}
const Button: FC<Props> = ({text, type, onClick, inverted = false}) => {
    return (
        <button
            className={`
                w-full font-[\'Archivo\'] border-black border-2 text-lg uppercase
                ${inverted ? 'bg-white' : 'bg-black'} py-1 px-3 transition duration-150
                ${inverted ? 'text-black' : 'text-white'} ${inverted ? 'hover:text-white' : 'hover:text-black'} 
                 relative ${inverted ? styles.inverted : styles.normal}`
            }
            type={type}
            onClick={(e) => onClick(e)}>
            <span className={"z-10 relative"}>{text}</span>
        </button>
    )

}

export default Button
