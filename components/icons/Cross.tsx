const Cross = ({ ...props }) => {
    return (
        <svg className={props.className} xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22" fill="none">
            <path d="M2 2L20 20" stroke="black" strokeWidth="4"/>
            <path d="M2 20L20 2" stroke="black" strokeWidth="4"/>
        </svg>
    )
}

export default Cross
