const Arrow = ({ ...props }) => {
    return (
        <svg className={props.className} xmlns="http://www.w3.org/2000/svg" width="13" height="20" viewBox="0 0 13 20" fill="none">
            <path d="M11 2L3 10L11 18" stroke="black" strokeWidth="3"/>
        </svg>
    )
}

export default Arrow
