import { FC } from 'react'
import Product from "@models/product.model";
import Button from "@components/Button";
import Image from "next/image";
import GetProduct from "@hooks/GetProduct";
import GetFormattedSize from "@hooks/GetFormattedSize";
import { v4 as uuid } from 'uuid';
import { useDispatch } from "react-redux";
import { addToCart } from "@redux/cart.slice";

interface Props {
    product: Product
}
const FeaturedProduct: FC<Props> = ({product}) => {
    const dispatch = useDispatch();
    const getRecommendedProducts = () => {
        const recommended: Product[] = [];
        product.details.recommendations.forEach(id => {
            const { data, loading } = GetProduct(`/api/products/${id}`)
            if (!loading && data) {
                recommended.push(data);
            }
        })
        return recommended;
    }
    return (
        <div className={"grid grid-cols-6 border-b-2 border-gray-300 py-12"}>
            {/* Title and add to cart row */}
            <h1 className={"font-bold text-2xl col-span-6 lg:col-span-5 order-1"}>{product.name}</h1>
            <div className={"w-full col-span-6 lg:col-span-1 lg:justify-self-end order-3 lg:order-2"}>
                <Button type={"submit"} text={"add to cart"} onClick={() => dispatch(addToCart(product))}></Button>
            </div>
            {/* Image row */}
            <div className={"w-full aspect-video max-h-[45vh] relative col-span-6 my-6 order-2 lg:order-3"}>
                <Image
                    src={product.image.src}
                    alt={product.image.alt}
                    fill={true}
                    style={{objectFit: 'cover'}}
                />
                <p className={"absolute bottom-0 left-0 text-black bg-white text-xl py-4 px-6 font-bold"}>
                    Photo of the day
                </p>
            </div>
            {/* Details row */}
            <div className={"col-span-6 lg:col-span-3 order-4 mt-6 lg:mt-0"}>
                <h2 className={"text-xl font-bold"}>
                    About the {product.name}
                </h2>
                <h3 className={"capitalize text-xl font-bold text-gray-400 py-3"}>
                    {product.category}
                </h3>
                <p className={"text-gray-400 font-normal"}>
                    {product.details.description}
                </p>
            </div>
            <div className={"col-span-6 lg:col-span-3 justify-self-end lg:text-right order-5 w-full mt-6 lg:mt-0"}>
                <h2 className={"text-xl font-bold"}>
                    People also buy
                </h2>
                <div className={"py-6 overflow-auto relative"}>
                    <div className={"flex flex-nowrap lg:flex-row-reverse gap-8"}>
                        {getRecommendedProducts().map(recommended => {
                            return (
                                <div className={"relative w-28 h-36 flex-none"} key={uuid()}>
                                    <Image
                                        src={recommended.image.src}
                                        alt={recommended.image.alt}
                                        fill={true}
                                        style={{objectFit: 'cover'}}
                                    ></Image>
                                </div>
                            )
                        })}
                    </div>
                </div>
                <h2 className={"text-xl font-bold pt-3"}>
                    Details
                </h2>
                <p className={"text-gray-400 font-normal py-3"}>Dimensions: {product.details.dimensions.width} x {product.details.dimensions.height} pixel</p>
                <p className={"text-gray-400 font-normal"}>Size: {GetFormattedSize(product.details.size)}</p>
            </div>
        </div>
    )

}

export default FeaturedProduct
