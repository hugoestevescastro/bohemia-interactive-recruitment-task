import Head from 'next/head'
import Product from "@models/product.model";
import {getPriceRanges, getProducts, getCategories} from "./api/products";
import PriceRange from "@models/price-range.model";
import FeaturedProduct from "@components/FeaturedProduct";
import Navbar from "@components/Navbar";
import {ProductList} from "@components/ProductList";
import { Filter, OnFilterModel } from "@components/Filter";
import {useEffect, useState} from "react";
import UseWindowSize from "@hooks/UseWindowsSize";
import {Filter as FilterIcon, Cross, Sort} from "@components/icons"
import Button from "@components/Button"
import {v4 as uuid} from "uuid"

export async function getServerSideProps() {
    const products = getProducts();
    const categories = getCategories()
    const priceRanges = getPriceRanges();
    return { props: { products, categories, priceRanges } }
}
interface HomeProps {
    products: Product[]
    categories: string[],
    priceRanges: PriceRange[]
}
export default function Home({products, categories, priceRanges}: HomeProps) {
    const sortParameters = ['price', 'alphabetic']
    /** Hook to get the window size, to change location of filters according to device */
    const windowSize = UseWindowSize();

    /** State of filter menu on mobile */
    const [isMobileFilterOpen, setIsMobileFilterOpen] = useState(false);

    /** Products used to display */
    const [displayedProducts, setDisplayedProducts] = useState<Product[]>(products);

    /** Boolean used to trigger clear filter */
    const [clearTrigger, setClearTrigger] = useState(false);

    /** Data structure used to store the filters */
    const [filter, setFilter] = useState<OnFilterModel | undefined>();

    /** Definition of sorting order */
    const [isSortAsc, setIsSortAsc] = useState(true);

    /** Definition of sorting parameter */
    const [sortParam, setSortParam] = useState(sortParameters[0]);

    /* Everytime the sort order or sort parameter changes, re-sort the products*/
    useEffect(() => {
        setDisplayedProducts(sortProducts(displayedProducts));
    },[isSortAsc, sortParam])

    /**
     * Sort the products according to the parameters
     * @returns sorted array
     */
    const sortProducts = (products: Product[]) => {
        let temporary;
        // If param is price, sort numerically, else defaults to alphabetically
        if (sortParam == 'price') {
            temporary = [...products].sort((a, b) => {
                return isSortAsc ?a.price-b.price : b.price - a.price
            });
        } else {
            temporary = [...products].sort((a, b) => {
                if (a.name < b.name) {
                    return -1;
                }
                if (a.name > b.name) {
                    return 1;
                }
                return 0;
            });
            !isSortAsc && temporary.reverse();
        }
        return temporary;
    }
    /**
     * Filters the products prop according to the given filter
     */
    const filterProducts = () => {
        let filtered = products.filter((product: Product) => {
            // Price Range filters
            let priceRange = true;
            if (filter && filter.priceRange) {
                // First case there is no start price, only end price
                const lessThan = !filter.priceRange.start && filter.priceRange.end && product.price <= filter.priceRange.end;
                // Normal case
                const normal = filter.priceRange.start && filter.priceRange.end && product.price >= filter.priceRange.start && product.price <= filter.priceRange.end;
                // Last case, when there is no end price, only start
                const moreThan = !filter.priceRange.end && filter.priceRange.start && product.price >= filter.priceRange.start;
                priceRange = (lessThan || normal || moreThan) as boolean;
            }
            // If there is no categories filters, assign the product, else check if it exists
            const categories = filter && filter.categories.length > 0 ? filter.categories.includes(product.category) : true;
            return (priceRange && categories) || !filter;
        })
        filtered = sortProducts(filtered);
        setDisplayedProducts(filtered)
        setClearTrigger(true);
    }

    /** Trigger a filter after setting the filter params, only in desktop */
    useEffect(() => {
        windowSize.width && windowSize.width >= 1024 && filterProducts();
    }, [filter])

    /** Featured product */
    const featured = products.find(p => p.featured);
      return (
        <>
          <Head>
            <title>Bejamas</title>
            <meta name="description" content="Recruitment task for Bohemia Interactive" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <link rel="icon" href="/favicon.ico" />
          </Head>
          <main className={"md:py-10 md:px-16 py-6 px-8"}>
              <Navbar />
              {featured && <FeaturedProduct product={featured} />}
              <div className={'flex py-12 flex-wrap relative justify-between'}>
                  <h1
                      className={'text-gray-300 font-normal text-lg lg:text-2xl'}>
                        <span className={'text-black font-bold'}>Photography</span>  /  Premium Photos
                  </h1>
                  {windowSize.width && windowSize.width < 1024 &&
                      <span onClick={() => {setIsMobileFilterOpen(!isMobileFilterOpen)}}>
                          <FilterIcon className={'w-7 cursor-pointer z-10'} />
                      </span>
                  }
                  {windowSize.width && windowSize.width >= 1024 &&
                      <div className={'flex gap-2 items-center'}>
                          <span
                              className={'cursor-pointer'}
                              onClick={()=> setIsSortAsc(!isSortAsc)}>
                                <Sort className={'w-3'} asc={isSortAsc} />
                          </span>
                          <p className={'text-gray-400 font-normal text-base'}>Sort By</p>
                          <select className={'capitalize font-normal text-base ml-4 -mt-0.5'} onChange={(e => setSortParam(e.target.value))}>
                              {sortParameters.map(param => {
                                  return <option key={uuid()} value={param}>{param}</option>
                              })}
                          </select>
                      </div>
                  }
                  <div className={'h-0 basis-full pb-12'} /> {/* This div is to create a break in the flex */}
                  {/* 1024px is tailwind's lg: breakpoint */}
                  {windowSize.width && windowSize.width >= 1024 && (
                      <div className={"w-1/3"}>
                          <Filter
                              categories={categories}
                              priceRanges={priceRanges}
                              onFilter={(filter: OnFilterModel) => {
                                  setFilter(filter);
                              }
                          } />
                      </div>
                  )}
                  {/* 1024px is tailwind's lg: breakpoint */}
                  <div className={'w-full lg:w-2/3'}>
                      <ProductList products={displayedProducts}/>
                  </div>
                  {/** If is before breakpoint, filters come from bottom*/}
                  {windowSize.width && windowSize.width < 1024 && (
                      <div className={`px-8 py-4 z-50 grid grid-cols-2
                        fixed bg-white w-screen left-0 h-auto transition-all duration-[350ms]
                        ${isMobileFilterOpen ? 'bottom-0' : '-bottom-full'}`}>
                            <h1 className={'text-2xl font-bold'}>Filter</h1>
                            <span className={'justify-self-end'} onClick={() => setIsMobileFilterOpen(false)}><Cross className={'w-7'} /></span>
                            <div className={'py-4 col-span-2 max-h-[70vh] overflow-y-auto'}>
                                <Filter
                                    categories={categories}
                                    priceRanges={priceRanges}
                                    clear={clearTrigger}
                                    onFilter={(filter: OnFilterModel) => {
                                        setFilter(filter);
                                        setClearTrigger(false)
                                    }
                                } />
                            </div>
                          <div className={'flex col-span-2 gap-2 border-t-2 border-gray-300 pt-4'}>
                              <Button type={"submit"} text={"clear"} onClick={() => setClearTrigger(true)} inverted={true}></Button>
                              <Button type={"submit"} text={"save"} onClick={() => {filterProducts(); setIsMobileFilterOpen(false)}}></Button>
                          </div>
                      </div>
                  )}
              </div>

          </main>
        </>
      )
}
