// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type {NextApiRequest, NextApiResponse} from 'next'
import Product from "@models/product.model";
import data from './database.json'

/**
 * Fetch a specific product in the database file
 * @return the product or undefined if not found
 */
export function getProduct(id: number): Product | undefined {
    return data.products.find(p => p.id === id);
}


export default function handler(
    req: NextApiRequest,
    res: NextApiResponse
) {
    // If the HTTP Method is not a get, do not allow it
    if (req.method !== 'GET') {
        res.setHeader('Allow', ['GET']);
        res.status(405).json({ message: `Method ${req.method} is not allowed` });
    }
    // Default case: send all products
    else {
        res.status(200).json(getProduct(Number(req.query.id)));
    }
}
