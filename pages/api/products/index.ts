// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type {NextApiRequest, NextApiResponse} from 'next'
import Product from "@models/product.model";
import PriceRange from "@models/price-range.model";
import data from './database.json'

/**
 * Fetch all the products in the database file
 * @return an array of products
 */
export function getProducts(): Product[] {
    return data.products;
}

/**
 * Fetch all the price ranges in the database file
 * @return an array of price ranges
 */
export function getPriceRanges(): PriceRange[] {
    return data.priceRange;
}


/**
 * Fetch all the categories of the products in the database file
 * @return an array of strings containing the categories
 */
export function getCategories(): string[] {
    return data.products.map((p: Product) => p.category).filter((value, index, self) => {
        return self.indexOf(value) === index
    })
}

export default function handler(
    req: NextApiRequest,
    res: NextApiResponse
) {
    // If the HTTP Method is not a get, do not allow it
    if (req.method !== 'GET') {
        res.setHeader('Allow', ['GET']);
        res.status(405).json({ message: `Method ${req.method} is not allowed` });
    }
    // If there is a query asking for priceRange specification
    else if (req.query.priceRangeSpecification === "true") {
        res.status(200).json(getPriceRanges())
    }
    // If there is a query asking for categories specification
    // (all the categories), send it according to previous method
    else if (req.query.categoriesSpecification === "true") {
        res.status(200).json(getCategories())
    }
    // Default case: send all products
    else {
        res.status(200).json(getProducts());
    }
}
