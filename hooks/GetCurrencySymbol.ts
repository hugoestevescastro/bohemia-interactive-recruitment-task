const GetCurrencySymbol = (currency: string) => {
    switch (currency.toLowerCase()) {
        case 'usd':
            return '$';
        case 'eur':
            return '€';
        default:
            return '€';
    }
}

export default GetCurrencySymbol;
